package LSBUlimits;

use strict;
use warnings;
use English;
use Data::Dumper;
use BSD::Resource;

our @ISA = ('Exporter');
our @EXPORT = qw (
                  &getUserLimits
                  &setUserLimits
                  &getAllUserLimits
                  &getLimitsResources
                  &getProcLimits
);

my $FNP_SUCCESS = 1;
my $FNP_FAIL = 0;
my $retValue = $FNP_FAIL;

# STATIC Resource names
#my @limitsList = qw /RLIMIT_CPU RLIMIT_FSIZE RLIMIT_DATA RLIMIT_STACK 
#                     RLIMIT_CORE RLIMIT_RSS RLIMIT_MEMLOCK RLIMIT_NPROC 
#                     RLIMIT_NOFILE RLIMIT_OFILE RLIMIT_OPEN_MAX RLIMIT_AS RLIMIT_VMEM/;

# Dynamic for current system
#my @limitsList = &getLimitsResources();

#################################################################################
# Gets ulimit (soft & hard) from the system for given resource
# Arg: $resource - one of the predefined value
# Returns a tiny hashreference with soft and hard limit of given resource
# for list of supported resources check getLimitsResources() func
# if the value is -1, then it is unlimited
# Won't work well on Solaris / HP machines
#################################################################################
sub getUserLimits
{
    my ($resource) = @_;
    my ($soft, $hard);
    my $proc_limit;
    my $resultHref;

    if (defined $resource && $resource !~ /^\s*$/)
    {
        ($soft, $hard) = getrlimit($resource);
        $resultHref->{$resource}->{'softlimit'} = $soft if (defined $soft);
        $resultHref->{$resource}->{'hardlimit'} = $hard if (defined $hard);

        print "[getUserLimits] For $resource: Current soft | hard limit is $soft | $hard\n\n";
        print "\nFor $resource : Current soft | hard limit is $soft | $hard";

    }
    else
    {
        print "Error: Resource parameter is missing in the argument list";
    }

    return $resultHref;
}

#############################################################################
# Function to log /proc/pid/limits data 
# Useful for verifying current process' ulimit values
# ###########################################################################
sub getProcLimits
{
    my ($filter) = @_;
    my $proc_limit;

    if (defined $filter && $filter !~ /^\s*$/)
    {
        chomp($proc_limit = `cat /proc/$$/limits | grep $filter`);
    }
    else
    {
        chomp($proc_limit = `cat /proc/$$/limits`);
    }

    if (defined $proc_limit)
    {
        print "===> /proc/$$/limits \n\t$proc_limit";
    }
    return $proc_limit;
}

###################################################################################
# Function to set given softlimit to given resource name (BSD::Resource format)
# for resource name format, check getLimitsResources() for details
# returns status as 1 or 0 for pass/fail respectively
###################################################################################
sub setUserLimits
{
    my ($resource, $newsoft, $newhard) = @_;
    my $status = $FNP_FAIL;

    if (!defined $resource || !defined $newsoft || !defined $newhard)
    {
        print "Required param missing to set user limit";
        print "\n[setUSerLimits] Missing required params (resource, newsoftlimit, newhardlimit)\n\n";
        return $status;
    }

    #print "\n[setUserLimits] ${resource}, Setting limits to $newsoft , $newhard\n";
    print "For ${resource}, Setting limits to $newsoft, $newhard";

    $status = setrlimit($resource, $newsoft, $newhard);

    if (defined $status && $status == $FNP_SUCCESS)
    {
        print "ulimit for ${resource} set succesfully to $newsoft , $newhard";
        print "\n*** [setUserLimits] ulimit for ${resource} set succesfully to $newsoft , $newhard\n";
    }
    else
    {
        print "Error: ulimit for ${resource} couldn't be set to $newsoft , $newhard";
        print "\n*** [setUserLimits] Error: ulimit for ${resource} couldn't be set to $newsoft , $newhard\n";
    }

    return $status;
}

################################################################################################
# Function which calls getUserLimits() to fetch values for all supported resources
# 1. Calls getLimitsResources() to get list of current system supported resource names
# 2. Loop through every supported resource names and fetch soft & hard limits
# 3. Store and returns hash of resource->hardlimit and $resource->softlimit for all resources
################################################################################################
sub getAllUserLimits
{
    my $returnHref;
    my $respHref;
    my $limitsListref = &getLimitsResources();
    my @limitsList = ();

    if (defined $limitsListref && ref($limitsListref) =~ /ARRAY/i)
    {
        @limitsList = @{$limitsListref};
    }

    if (@limitsList)
    {
        foreach my $resource (@limitsList)
        {
            $respHref = &getUserLimits($resource);
            if (defined $respHref && $respHref->{$resource})
            {
                $returnHref->{$resource} = $respHref->{$resource};
            }
            else
            {
                print "No ulimit info for $resource, ignoring..";
                print "[getAllUserLimits] No ulimit info for $resource, ignoring..\n";
            }
        }
    }

    return $returnHref;
}

#########################################################################################
# Function to get all the available/supported ulimits resources (cpu use/stack size/fd..)
# What limits are available depends on the operating system, check OS help for details
# returns the list of supported resource names in BSD::Resource format (RLIMIT_FSIZE,..)
##########################################################################################
sub getLimitsResources
{
    # This gets the hash reference to current sys supported limits resources
    my $resHref = get_rlimits();
    
    my @keys;
    if (defined $resHref && ref($resHref) =~ /HASH/i)
    {
        @keys = keys %{$resHref};
        print "Supported limits resources: @keys";
        print "\n[getLimitsResources] Current System Supported Limits:\n\t@keys\n\n";
    }

    return \@keys;
}
    
1;
