#!/usr/bin/env perl

################################################################################
#  This script is a profiler script to demonstrate LSBUlimits.pm library
################################################################################
#
# Description :
# - Fetch ulimit (soft and hard) for a resource [FD]
# - Fetch list of supported ulimit resources
# - Fetch ulimit (soft and hard) for all the supported resources in the system
# - Set ulimit for given resource [FD]
# - Verify by fetching ulimit for the same resource and from /proc/$$/ulimit data
# - Run A File Descriptor Simulation to breach the set fd limit
# - Won't be needed to reset/restore the modified limits for the resource (local)
#
# Note: The script expects the machine to be configured with,
# fd soft/hard limit as 1024/65536 atleast
# Else, it dynamically adjusts to the limits of the machine
################################################################################

use strict;
use warnings;
use Data::Dumper;
use Time::HiRes;

my $FNP_SUCCESS = 1;
my $FNP_FAIL = 0;
my $retValue = $FNP_FAIL;

################################################################################
# Setup for test case
################################################################################
my ($newlimit, $totalfiles) = (shift, shift);
$newlimit = 1024 if (!defined $newlimit);
$totalfiles = 32 if (!defined $totalfiles);

my $resource = 'RLIMIT_NOFILE';
my $respHref;
my $proc_limit;
my $status;

$respHref = getUserLimits($resource);
print "\n" . Dumper($respHref) . "\n";

$proc_limit = getProcLimits("files");
print "\n===> /proc/$$/limits \n\t$proc_limit\n\n";

$respHref = getAllUserLimits();
print "\n" . Dumper($respHref) . "\n";

$proc_limit = getProcLimits();
print "\n===> /proc/$$/limits \n\t$proc_limit\n\n";

$status = setUserLimits($resource, $newlimit, $respHref->{$resource}->{'hardlimit'});
print "\n*** setUserLimits for $resource Status: $status\n\n";

$respHref = getUserLimits($resource);
print "\n" . Dumper($respHref) . "\n";

$proc_limit = getProcLimits("files");
print "\n===> /proc/$$/limits \n\t$proc_limit\n\n";

########### Call to sub-routine after modifying ulimits ###########
&fdSimulate($totalfiles);

## END OF SCRIPT

## ________________ PROFILER SUB ROUTINE __________ ####

sub fdSimulate
{
    my $total = shift;
    print "Total Number of fd to be opened $total for testing\n";
    my $sleeptime = 0.1;
    $sleeptime = 0.05 if ($total <= 100);

    my ($i, $j) = (1,1);
    my $retVal;
    my @filelist;
    my $filename = "test";

    # No Buffering
    select( (select(), $| = 1)[0] );

    chdir ("/tmp/limittest");
    print "\nOpening ($total) files to write for increasing file descriptors usage...\n";

    for ($i = 0; $i < $total; $i++)
    {
        print "$i , " ;
        $filename = "test" . "$i";
        if (open my $in, ">", ${filename})
        {
            push @filelist, $in;
            print $in $filename . "\n";
            Time::HiRes::sleep($sleeptime); # seconds
        }
        else
        {
            print "Error after opening $i files from this script\n";
            warn "\nError: Could not open file '$filename'. $!";
            last;
        }
    }

    print "\n\n*** Totally $i File Handles opened from this script ***\n";

    for ($j = 0; $j < $i; $j++)
    {
        $retVal = pop @filelist;
        #print "$retVal , ";
        close($retVal);
        Time::HiRes::sleep($sleeptime); #.1 seconds
    }
 
   ($i == $j) ? print "\nClosed all the opened filehandles\n" : print "\nNot all filehandles got closed..\n";
}

print " ---- End of Profiler --- That's all folks -----\n\n";

exit 1;

