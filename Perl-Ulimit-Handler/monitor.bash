#!/bin/bash

echo -e "current working directory: " $(pwd)
echo "$@"

## Live monitor to watch fd usage when triggering profiler script
## USAGE: monitor.bash ./ProfilerScript.pl 1024 998

"$@" > ./out_ptest.txt &
pid=$!
echo -e "\npid of ptest.pl: ${pid}\n"

HOW_MANY=0
MAX=0
MIN=99999999
while [ -r "/proc/${pid}" ]; 
do
    HOW_MANY=`lsof -p ${pid} 2>/dev/null| wc -l`
    #output for live monitoring
    echo `date +%H:%M:%S` "No. of open fd: ${HOW_MANY}"

    # look for max value so far
    if [ $MAX -lt $HOW_MANY ]; then
        let MAX=$HOW_MANY
        echo -e "new max is ${MAX}"
    fi

    # look fo min value so far
    #if [[ $HOW_MANY -lt $MAX && $MIN -gt $HOW_MANY ]]; then
    #    let MIN=$HOW_MANY
    #    echo -e "new min is ${MIN}"
    #fi 

    # test every second
    sleep 1
done
echo -e "\nmaximum fd count was ${MAX}\n"

echo -e "\n--------------------------------------------------------------------------------------\n";
echo -e "Output of $@"
cat out_ptest.txt
echo -e "\n--------------------------------------------------------------------------------------\n";
