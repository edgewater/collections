#!/usr/bin/python3

import re
from collections import defaultdict

####################
# ifconfig data can also be pulled from os.subprocess
###################



ifconData = [
'ath0      IEEE 802.11ng  ESSID:"JioFiber-bB3uF"  ',
"          Mode:Master  Frequency:2.412 GHz  Access Point: 80:CA:4B:55:0D:66  ",
"          Bit Rate:192 Mb/s   Tx-Power:15 dBm  ",
"          RTS thr:off   Fragment thr:off",
"          Encryption key:72A4-E216-2F8E-23AB-E9FE-FFA9-AB66-4096   Security mode:restricted",
"          Power Management:off",
"          Link Quality=0/94  Signal level=-98 dBm  Noise level=-98 dBm (BDF averaged NF value in dBm)",
"          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0",
"          Tx excessive retries:0  Invalid misc:0   Missed beacon:0",
"",
'ath1      IEEE 802.11ac  ESSID:"JioFiber-bB3uF_5G"  ',
"          Mode:Master  Frequency:5.745 GHz  Access Point: 80:CA:4B:55:0D:67  ",
"          Bit Rate:866.7 Mb/s   Tx-Power:26 dBm  ",
"          RTS thr:off   Fragment thr:off",
"          Encryption key:A2D6-D4D1-72D7-347C-FE74-15F0-07A2-55E6   Security mode:restricted",
"          Power Management:off",
"          Link Quality=0/94  Signal level=-106 dBm  Noise level=-106 dBm (BDF averaged NF value in dBm)",
"          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0",
"          Tx excessive retries:0  Invalid misc:0   Missed beacon:0",
"",
"lo        no wireless extensions.",
"",
"eth0      no wireless extensions.",
"",
'ath01     IEEE 802.11ng  ESSID:"JioFiber-bB3uF-BH"  ',
"          Mode:Master  Frequency:2.412 GHz  Access Point: 86:CA:4B:55:0D:66  ",
"          Bit Rate:192 Mb/s   Tx-Power:15 dBm  ",
"          RTS thr:off   Fragment thr:off",
"          Encryption key:F478-D0EF-F5DC-9CAA-EC35-3C50-7E5D-2742   Security mode:restricted",
"          Power Management:off",
"          Link Quality=0/94  Signal level=-98 dBm  Noise level=-98 dBm (BDF averaged NF value in dBm)",
"          Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0",
"          Tx excessive retries:0  Invalid misc:0   Missed beacon:0",
]


#for i in range(len(ifconData)):
#    print (ifconData[i])

def newLookup(inpArray):
    lineDict = defaultdict(dict)
    INF = ""
    print ("Num of Lines in ifconData:", len(inpArray))
    for i in range(0,len(inpArray)):
        currLine = inpArray[i]
        #print (currLine)
        
        # Skip blanklines
        patternBlank = re.compile("^\s*$")
        matchRes = patternBlank.match( currLine )
        if matchRes:
            #print (INF, "blankline.. Skipping")
            continue
        else:
            #Skip RX, TX lines
            patternRX = re.compile("^\s*RX|^\s*TX", flags=re.I)
            matchRes = patternRX.match( currLine )
            if matchRes:
                #print (INF, "Skipping TX, RX lines", currLine)
                continue
            else:
                # Pattern for fetching INTERFACE INF
                patternINF = re.compile("^([0-9A-Za-z]+)\s+")
                matchRes = patternINF.match( currLine )
                if matchRes:
                    INF = matchRes.group(1)
                    print (INF)
                    patternESS = re.compile("^([0-9A-Za-z]+)\s+.*ESSID:\"(.*)\"")
                    matchRes = patternESS.match( currLine )
                    if matchRes:
                        ESSIDVal = matchRes.group(2)
                        print ("********* ESSID: ", ESSIDVal, "**********")
                        lineDict[INF]['ESSID'] = ESSIDVal
                    else:
                        print("No ESSID in interface line:", currLine)
                    continue
                else:
                    # Split current data line for key-value pair processing
                    currLine = currLine.strip()
                    splitArray = currLine.split('  ');
                    #print ("Length of split Array for currLine", len(splitArray))
                    #for i in range(len(splitArray)):
                    #    print (splitArray[i],"/-/")
                        
                    j = 0
                    while j < len(splitArray):
                        patternSpecial = re.compile("(.*?):(.*)")
                        matchRes = patternSpecial.match( splitArray[j] )
                        if matchRes:
                            key = matchRes.group(1)
                            value = matchRes.group(2)
                            
                            j = j + 1
                            print ("[%d/%d] Key: %s, Value: %s"%(j, len(splitArray),key, value));
                            
                            lineDict[INF][key] = value
                            continue
                        else:
                            patternSpecial = re.compile("(.*?)=(.*)")
                            matchRes = patternSpecial.match( splitArray[j] )
                            if matchRes:
                                key = matchRes.group(1)
                                value = matchRes.group(2)
                                
                                j = j + 1
                                print ("[%d/%d] Key: %s, Value: %s"%(j, len(splitArray),key, value));
                                
                                lineDict[INF][key] = value
                                continue
                        j= j + 1
                        # continue with next iteration of while
    return lineDict

# Request lookup table with ifconfig data
ifconDict = newLookup(ifconData)
print("\n\n", (str(ifconDict).replace(",", ",\n")).replace("},","},\n"))

