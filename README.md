This directory collects some of the sophisticated programs that I developed for any specific problem or a hobby. These are usually stripped of any official use and developed for learning.

1. CiscoRouterParser
    * Parse Cisco router information from ciscoutil command output to extract interface meta data (ifName, ESSID, Bit Rate,..)
    * Looks up with Regex to fetch key-value pairs and then stores the parsed data in Python Dictionary

2. IfconfigParser
    * Parse Unix based ifconfig command output for interface meta data (ifName, netmask, mac address,..)
    * Regex parser to fetch key-value pairs and stores in Python dictionary

3. Ulimit Handler
    * Perl Library to get/set softlimit value of supported ulimit resources (fd, mem, core, stacksize..)
    * Profiler script for modifying fd limit and test breaching the resource limit
    * Library will be useful to call funcitons/external commands/remote methods with modified ulimit values
