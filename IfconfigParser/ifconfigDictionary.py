#!/usr/bin/python3

import re
from collections import defaultdict

####################
# ifconfig data can also be pulled from os.subprocess
###################

ifconData = ["enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500",
"        inet 10.80.130.43  netmask 255.255.254.0  broadcast 10.80.131.255",
"        inet6 fe80::a00:27ff:fedf:d19e  prefixlen 64  scopeid 0x20<link>",
"        ether 08:00:27:df:d1:9e  txqueuelen 1000  (Ethernet)",
"        RX packets 2434563  bytes 2843290362 (2.6 GiB)",
"        RX errors 0  dropped 0  overruns 0  frame 0",
"        TX packets 915261  bytes 90560385 (86.3 MiB)",
"        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0",
"",
"lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536",
"        inet 127.0.0.1  netmask 255.0.0.0",
"        inet6 ::1  prefixlen 128  scopeid 0x10<host>",
"        loop  txqueuelen 1000  (Local Loopback)",
"        RX packets 28122  bytes 4616756 (4.4 MiB)",
"        RX errors 0  dropped 0  overruns 0  frame 0",
"        TX packets 28122  bytes 4616756 (4.4 MiB)",
"        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0",
"",
"virbr0: flags=4099<UP,BROADCAST,MULTICAST>  mtu 1500",
"        inet 192.168.122.1  netmask 255.255.255.0  broadcast 192.168.122.255",
"        ether 52:54:00:3d:77:0a  txqueuelen 1000  (Ethernet)",
"        RX packets 0  bytes 0 (0.0 B)",
"        RX errors 0  dropped 0  overruns 0  frame 0",
"        TX packets 0  bytes 0 (0.0 B)",
"        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0",
""]

#for i in range(len(ifconData)):
#    print (ifconData[i])


def createLookup(inpArray):
    lineDict = defaultdict(dict)
    INF = ""
    print ("Num of Lines in ifconData:", len(inpArray))
    for i in range(0,len(inpArray)):
        currLine = inpArray[i]
        print (currLine)
        
        # Skip blanklines
        patternBlank = re.compile("^\s*$")
        matchRes = patternBlank.match( currLine )
        if matchRes:
            print (INF, "blankline.. Skipping")
            continue
        else:
            #Skip RX, TX lines
            patternRX = re.compile("^\s*RX|^\s*TX")
            matchRes = patternRX.match( currLine )
            if matchRes:
                print (INF, "Skipping RX line", currLine)
                continue
            
            else:
                # Pattern for fetching INTERFACE INF
                patternINF = re.compile("^([0-9A-Za-z]+)\:\s+")
                matchRes = patternINF.match( currLine )
                if matchRes:
                    INF = matchRes.group(1)
                    print (INF, "Interface Found in", currLine)
                    continue
                else:
                    # Split current data line for key-value pair processing
                    currLine = currLine.strip()
                    splitArray = currLine.split(' ');
                    print ("Length of split Array for currLine", len(splitArray))
                    for i in range(len(splitArray)):
                        print (splitArray[i],"/-/")
                    
                    j = 0
                    while j < len(splitArray):
                        #print ("Current Line: ", splitArray[j])
                        
                        # Ignore bracketword totally
                        patternSpecial = re.compile(".*\)|\(.*")
                        matchRes = patternSpecial.match( splitArray[j] )
                        if matchRes:
                            print ("Matched bracketword: ", splitArray[j])
                            j = j + 1
                            continue
                        else:
                            # move next if key is empty. If value is empty, that will be assigned as value
                            patternSpace = re.compile("^\s*$")
                            matchRes = patternSpace.match( splitArray[j] )
                            if matchRes:
                                print ("Matched empty space for key, moving next")
                                j = j + 1
                                continue
                            else:
                                key = splitArray[j]
                                value = splitArray[j+1]

                                j = j + 2
                                print ("Key: %s, Value: %s"%(key, value));

                                lineDict[INF][key] = value
                                continue
                            
                            j = j + 1
                    else:
                        print ("Fetching Next record for", INF)
                
    return lineDict

# Request lookup table with ifconfig data
ifconDict = createLookup(ifconData)
print("\n\n", (str(ifconDict).replace(",", ",\n")).replace("},","},\n"))
